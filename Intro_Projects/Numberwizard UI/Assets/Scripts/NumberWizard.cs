using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour {

    [SerializeField] Text hypothosysis;

    int max = 1001;
    int min = 0;
    int guess =  500;

    void Start(){
        hypothosysis.text = guess.ToString();
    }

    public void ToHigh(){
        max = guess + 1;
        guess = (max + min) / 2;
        hypothosysis.text = guess.ToString();
    }

    public void ToLow(){
        min = guess;
        guess = (max + min) / 2;
        hypothosysis.text = guess.ToString();
    }

    public void Correct(){
        guess = 500;
        min = 0;
        max = 1001;
        hypothosysis.text = guess.ToString();
    }

    void update()
    {
        hypothosysis.text = guess.ToString();
    }
}