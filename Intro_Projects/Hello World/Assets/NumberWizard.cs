using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class NumberWizard : MonoBehaviour {

    int max = 1000;
    int min = 1;
    int guess = 500;

    public void ToHigh()
    {
        max = guess + 1;
        guess = (max + min) / 2;
    }

    public void ToLow(){
        min = guess;
        guess = (max + min) / 2;
    }

    public void Correct(){
        guess = 500;
        min = 1;
        max = 1000;
    }
}

   